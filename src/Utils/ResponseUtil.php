<?php

namespace ErpPos\Utils;

class ResponseUtil
{
    function __construct()
    {
    }

    public function GenerateResponse($status, $data, $pagination, $error_message, $statusCode = 200)
    {
        $response = array(
            "success" => $status,
		);
		
		if($pagination != null)
		{
			$response["pagination"] = $pagination;
		}

		if($error_message != null)
		{
			$response["error"] = $error_message;
		}

		if($data != null)
		{
			$response["data"] = $data;
		}
        
        return response(json_encode($response), $statusCode)->header('Content-Type', 'application/json');
    }
}