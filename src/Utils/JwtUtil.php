<?php

namespace ErpPos\Utils;

class JwtUtil
{
	// JWT Header
	private $header = [
		"alg"     => "HS256",
		"typ"     => "JWT"
	];

	private $algo = 'sha256';

    function __construct()
    {
    }

    public function GenerateJWT($payload, $secret) {
		$headerEncoded = $this->base64UrlEncode(json_encode($this->header));
		$payloadEncoded = $this->base64UrlEncode(json_encode($payload));
	 
		// Delimit with period (.)
		$dataEncoded = "$headerEncoded.$payloadEncoded";
	 
		$rawSignature = hash_hmac($this->algo, $dataEncoded, $secret, true);
	 
		$signatureEncoded = $this->base64UrlEncode($rawSignature);
	 
		// Delimit with second period (.)
		$jwt = "$dataEncoded.$signatureEncoded";
	 
		return $jwt;
	} 

	private function base64UrlEncode($data)
	{
		$urlSafeData = strtr(base64_encode($data), '+/', '-_');
	
		return rtrim($urlSafeData, '='); 
	} 
}